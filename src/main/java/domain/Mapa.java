package domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Mapa {

    private static Mapa INSTANCIA;
    
    static {
        HashMap<String, Ciudad> m = new HashMap<String, Ciudad>();
        INSTANCIA = new Mapa(m);
    }

    public static Mapa getMapa() {
        return INSTANCIA;
    }

	private Map<String, Ciudad> ciudades;
	private Set<Ruta> rutas;
	
	Mapa(Map<String, Ciudad> ciudades) {
		this.checkCiudades(ciudades);
		this.ciudades = ciudades;
		this.rutas = new HashSet<Ruta>();
	}
	
	Mapa(Map<String, Ciudad> ciudades, Set<Ruta> rutas) {
		this.checkCiudades(ciudades);
		this.checkRutas(rutas);
		this.ciudades = ciudades;
		this.rutas = rutas;
		//this.crearConexiones();
	}
	
	private void checkCiudades(Map<String, Ciudad> ciudades) {
		if( ciudades == null )
			throw new IllegalArgumentException("Ciudades no puede ser null.");
	}
	
	private void checkRutas(Set<Ruta> rutas) {
		if( rutas == null )
			throw new IllegalArgumentException("Rutas no puede ser null.");
	}
	
	private void checkCiudad(Ciudad Ciudad) {
		if( Ciudad == null )
			throw new IllegalArgumentException("Ciudad no puede ser null.");
	}
	
	private void checkRuta(Ruta Ruta) {
		if( Ruta == null )
			throw new IllegalArgumentException("Ruta no puede ser null.");
	}
	
	private void checkNombre(String nombre) {
		if( nombre == null )
			throw new IllegalArgumentException("Nombre no puede ser null.");
	}
	
	private void contieneCiudad(Ciudad Ciudad) {
		if(!this.ciudades.containsValue(Ciudad)) {
			throw new IllegalArgumentException("El Ciudad debe existir dentro del mapa.");
		}
	}
	
	private void contieneRuta(Ruta Ruta) {
		if(!this.rutas.contains(Ruta)) {
			throw new IllegalArgumentException("La Ruta debe existir dentro del mapa.");
		}
	}
	
	public int cantCiudades() {
		return this.ciudades.size();
	}

	public int cantRutas() {
		return this.rutas.size();
	}

	public int cantVecinos(Ciudad ciudad) {
		this.checkCiudad(ciudad);
		this.contieneCiudad(ciudad);
		return getVecinos(ciudad).size();
	}

	public void addRuta(Ruta Ruta) {
		this.checkRuta(Ruta);
		this.rutas.add(Ruta);
	}

	public void removeRuta(Ruta Ruta) {
		this.checkRuta(Ruta);
		this.contieneRuta(Ruta);
		this.rutas.remove(Ruta);
	}

	public void addCiudad(Ciudad Ciudad) {
		this.checkCiudad(Ciudad);
		this.ciudades.put(Ciudad.getNombre(), Ciudad);
	}

	public void removeCiudad(Ciudad Ciudad) {
		this.checkCiudad(Ciudad);
		this.contieneCiudad(Ciudad);
		this.eliminarConexiones(Ciudad);
		this.ciudades.remove(Ciudad.getNombre());
	}

	public Set<Ciudad> getVecinos(Ciudad Ciudad) {
		this.checkCiudad(Ciudad);
		this.contieneCiudad(Ciudad);
		return rutas.stream()
		            .filter(r -> r.esExtremo(Ciudad))
		            .flatMap(r -> r.getExtremos().stream())
		            .filter(r -> !r.equals(Ciudad))
		            .collect(Collectors.toSet());
	}

	/*
	 * Lista de vecinos ordenada según comparador.
	 */
	public List<Ciudad> getVecinosOrdenados(Ciudad ciudad, Comparator<Ciudad> comparador) {
	    List<Ciudad> lista = new ArrayList<Ciudad>(getVecinos(ciudad));
	    lista.sort(comparador);
	    return lista;
	}

	/*
	 * Comparador para que el método getVecinosOrdenados devuelva una lista
	 * ordenada según ruta más barata de menor a mayor.
	 */
	public Comparator<Ciudad> getComparatorVecinosCostoMenorAMayor(Ciudad ciudad) {
	    return (c1, c2) -> getRutaMasBarata(ciudad, c1).getCosto()
	            .compareTo(getRutaMasBarata(ciudad, c2).getCosto());
	}

	/*
	 * Comparador para que el método getVecinosOrdenados devuelva una lista
	 * ordenada según ruta más barata de mayor a menor.
	 */
	public Comparator<Ciudad> getComparatorVecinosCostoMayorAMenor(Ciudad ciudad) {
	    return (c1, c2) -> {
	        return -getRutaMasBarata(ciudad, c1).getCosto()
	                .compareTo(getRutaMasBarata(ciudad, c2).getCosto());
	    };
	    /*return (c1, c2) -> {
	        
            if (getRutaMasBarata(ciudad, c1).getCosto()
                    .compareTo(getRutaMasBarata(ciudad, c2).getCosto()) == -1)
                return 1;
            else if (getRutaMasBarata(ciudad, c1).getCosto()
                    .compareTo(getRutaMasBarata(ciudad, c2).getCosto()) == 1)
                return -1;
            else
                return 0;
        }; */
	}

	public Set<Ruta> getRutasEntre(Ciudad ciudad1, Ciudad ciudad2) {
	    return rutas().stream()
	                  .filter(r -> r.esExtremo(ciudad1) && r.esExtremo(ciudad2))
	                  .collect(Collectors.toSet());
	}

	public Ruta getRutaMasBarata(Ciudad c1, Ciudad c2) {
	    return getRutasEntre(c1, c2).stream()
	                                .min((r1, r2) -> r1.getCosto().compareTo(r2.getCosto()))
	                                .get();
	}

	public Set<Ruta> rutas() {
		return new HashSet<Ruta>(this.rutas);
	}

	public Set<Ciudad> ciudades() {
		Set<Ciudad> Ciudads = new HashSet<Ciudad>(this.ciudades.values());
		return Ciudads;
	}

	public Collection<String> getNombresDeCiudades() {
	    return new HashSet<String>(ciudades.keySet());
	}

	public Ciudad getCiudad(String nombre) {
		this.checkNombre(nombre);
		return this.ciudades.get(nombre);
	}

	private void eliminarConexiones(Ciudad Ciudad) {
		Set<Ruta> aux = new HashSet<Ruta>();
		for(Ruta r : this.rutas) {
			if(r.getExtremo1().equals(Ciudad) || r.getExtremo2().equals(Ciudad)) {
				aux.add(r);
			}
		}
		this.rutas.removeAll(aux);
	}
	
}