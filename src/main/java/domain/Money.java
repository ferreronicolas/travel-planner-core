package domain;

import java.math.BigDecimal;


public class Money implements Comparable<Money> {
    
	private BigDecimal monto;
	public static final Money MAX  = new Money(BigDecimal.valueOf(Long.MAX_VALUE));
	public static final Money CERO = new Money("0");
	public static final Money MONTO_DESCONOCIDO = new Money(new BigDecimal(0)) {
	    @Override
	    public String toString() {
	        return "Monto desconocido";
	    }
	    
	    @Override
	    public Money sumar(Money dinero) {
	        return this;
	    }
	    
	    @Override
	    public Money restar(Money dinero) {
	        return this;
	    }
	};
	
	public Money(BigDecimal monto) {
		if(monto==null) {
			throw new IllegalArgumentException("El monto ingresado es nulo");
		}else {
			this.monto = monto;
		}
	}
	
	public Money(String monto) {
		if(monto==null) {
			throw new IllegalArgumentException("El monto ingresado es nulo");
		}else {
			if (monto.matches("\\d+(\\.\\d+)?")){
				this.monto = new BigDecimal(monto);
			} else {
				throw new IllegalArgumentException("El string debe tener formato: numero.decimales");
			}
		}	
	}

	public BigDecimal getMonto() {
		return monto;
	}

	@Override
	public String toString() {
		return this.monto.toString();
	}

	@Override
	public int hashCode() {
		final int primo = 31;
		return primo * monto.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Money otro = (Money) obj;
		if (!monto.equals(otro.monto))
			return false;
		return true;
	}
	
	public Money sumar(Money dinero) {
		if(dinero==null) {
			throw new IllegalArgumentException("El monto ingresado es nulo");
		}else {
		return new Money(this.monto.add(dinero.getMonto()));
		}
	}
	
	public Money restar(Money dinero) {
		if(dinero==null) {
			throw new IllegalArgumentException("El monto ingresado es nulo");
		}else {
		return new Money(this.monto.subtract(dinero.getMonto()));
		}
	}

    @Override
    public int compareTo(Money arg0) {
        return monto.compareTo(arg0.getMonto());
    }

	
}