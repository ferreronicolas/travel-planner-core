package domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

public class Ruta {

    private String nombre;
	private Ciudad extremo1;
	private Ciudad extremo2;
	private Money  costo;
	private BigDecimal distancia;
	
	public Ruta(Ciudad desde, Ciudad hacia, int distancia) {
		constructor(desde, hacia, new BigDecimal(distancia));
	}
	
	public Ruta(Ciudad desde, Ciudad hacia, double distancia) {
		constructor(desde, hacia, new BigDecimal(distancia));
	}
	
	public Ruta(Ciudad desde, Ciudad hacia, int distancia, Money costo) {
		constructor(desde, hacia, new BigDecimal(distancia));
		this.costo = costo;
	}
	
	public Ruta(Ciudad desde, Ciudad hacia, double distancia, Money costo) {
		constructor(desde, hacia, new BigDecimal(distancia));
		this.costo = costo;
	}
	
	private void constructor(Ciudad desde, Ciudad hacia, BigDecimal distancia) {
		checkCiudads(desde, hacia);
		checkDistancia(distancia);
		this.extremo1 = desde;
		this.extremo2 = hacia;
		this.distancia = distancia;
	}
	
	private void checkCiudads(Ciudad desde, Ciudad hacia) {
		if( desde == null )
			throw new IllegalArgumentException("Desde no puede ser null.");
		if( hacia == null )
			throw new IllegalArgumentException("Hacia no pude ser null.");
		if( desde.equals(hacia) )
			throw new IllegalArgumentException("Ciudads no pueden ser iguales.");
	}
	
	private void checkDistancia(BigDecimal distancia) {
		if( distancia.compareTo(BigDecimal.ZERO) == 0 )
			throw new IllegalArgumentException("Distancia no puede ser cero.");
		if( distancia.compareTo(BigDecimal.ZERO) == -1 )
			throw new IllegalArgumentException("Distancia no puede ser negativo.");
	}

	public BigDecimal getPeso() {
		return this.distancia;
	}

	public Ciudad getExtremo1() {
		return this.extremo1;
	}

	public Ciudad getExtremo2() {
		return this.extremo2;
	}

	public boolean esExtremo(Ciudad ciudad) {
	    return extremo1.equals(ciudad) || extremo2.equals(ciudad);
	}

	public Collection<Ciudad> getExtremos() {
	    Collection<Ciudad> extremos = new ArrayList<Ciudad>(2);
	    extremos.add(extremo1);
	    extremos.add(extremo2);
	    return extremos;
	}

	public Money getCosto() {
		return this.costo;
	}

	public void setNombre(String nombre) {
	    this.nombre = nombre;
	}

	public String getNombre() {
	    return nombre;
	}

	public void setCosto(Money costo) {
		this.costo = costo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((costo == null) ? 0 : costo.hashCode());
		result = prime * result + extremo1.hashCode();
		result = prime * result + distancia.hashCode();
		result = prime * result + extremo2.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ruta other = (Ruta) obj;
		if (costo == null) {
			if (other.costo != null)
				return false;
		} else {
			if (other.costo == null)
				return false;
			else if (!costo.equals(other.costo))
				return false;
		}
		if (!extremo1.equals(other.extremo1))
			return false;
		if (!extremo2.equals(other.extremo2))
			return false;
		if (!distancia.equals(other.distancia))
			return false;
		return true;
	}
	
}