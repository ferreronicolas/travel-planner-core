package domain;

public class Alojamiento {

    public static Alojamiento getAlojamientoNulo() {
        Alojamiento ret = new Alojamiento("Alojamiento no encontrado",
                                          Money.CERO);
        return ret;
    }

	private String nombre;
	private Money costoPorDia;
	
	public Alojamiento(String nombre, Money costo) {
		if( nombre == null )
			throw new IllegalArgumentException("Nombre no puede ser null.");
		if( costo == null )
			throw new IllegalArgumentException("Costo no puede ser null.");
		this.nombre = nombre;
		this.costoPorDia = costo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public Money getCostoPorDia() {
		return this.costoPorDia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + costoPorDia.hashCode();
		result = prime * result + nombre.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alojamiento other = (Alojamiento) obj;
		if (!costoPorDia.equals(other.costoPorDia))
			return false;
		if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

}
