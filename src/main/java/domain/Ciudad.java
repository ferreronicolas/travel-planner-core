package domain;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Ciudad {

    private String           nombre;
	private Set<Alojamiento> alojamientos;
	private Set<Atraccion>   atracciones;
	
	public Ciudad(String nombre) {
		if( nombre == null )
			throw new IllegalArgumentException("Nombre no puede ser null.");
		this.nombre       = nombre;
		this.atracciones  = new HashSet<Atraccion>();
		this.alojamientos = new HashSet<Alojamiento>();
	}
	
	public String getNombre() {
		return nombre;
	}

	public void addAlojamiento(Alojamiento alojamiento) {
		if( alojamiento == null )
			throw new IllegalArgumentException("ALojamiento no puede ser null.");
		this.alojamientos.add(alojamiento);
	}
	
	public void removeAlojamiento(Alojamiento alojamiento) {
		if( alojamiento == null )
			throw new IllegalArgumentException("Alojamiento no puede ser null.");
		alojamientos.remove(alojamiento);
	}
	
	public Set<Alojamiento> getAlojamientos() {
		return alojamientos.stream()
						   .collect(Collectors.toSet());
	}

	public Set<String> getNombresDeAlojamientos(){
	    return alojamientos.stream()
	                       .map(a -> a.getNombre())
	                       .collect(Collectors.toSet());
	}

	public Alojamiento getAlojamiento(String nombre) {
	    return alojamientos.stream()
	                       .filter(a -> a.getNombre().equals(nombre))
	                       .findAny()
	                       .orElse(Alojamiento.getAlojamientoNulo());
	}

	public void removeAtraccion(Atraccion atraccion) {
		if( atraccion == null )
			throw new IllegalArgumentException("Atracción no puede ser null.");
		atracciones.remove(atraccion);
	}
	
	public void addAtraccion(Atraccion atraccion) {
		if( atraccion == null )
			throw new IllegalArgumentException("Atracción no puede ser null.");
		this.atracciones.add(atraccion);
	}
	
	public Set<Atraccion> getAtracciones() {
		return atracciones.stream()
				 		  .collect( Collectors.toSet() );
	}

	public Atraccion getAtraccion(String nombre) {
	    return atracciones.stream()
	                      .filter(a -> a.getNombre().equals(nombre))
	                      .findAny()
	                      .orElse(Atraccion.getAtraccionNula());
	}

	public Set<String> getNombresDeAtracciones() {
	    return atracciones.stream()
	                      .map(a -> a.getNombre())
	                      .collect(Collectors.toSet());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + alojamientos.hashCode();
		result = prime * result + atracciones.hashCode();
		result = prime * result + nombre.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ciudad other = (Ciudad) obj;
		if (!alojamientos.equals(other.alojamientos))
			return false;
		if (!atracciones.equals(other.atracciones))
			return false;
		if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

}