package domain;

public class Atraccion {

    public static Atraccion getAtraccionNula() {
        Atraccion ret = new Atraccion("Atraccion nula", Money.CERO);
        return ret;
    }

	private String nombre;
	private Money costoDeEntrada;
	
	public Atraccion(String nombre, Money costo) {
		if( nombre == null )
			throw new IllegalArgumentException("Nombre no puede ser null.");
		if( costo == null )
			throw new IllegalArgumentException("Costo no puede ser null.");
		this.nombre = nombre;
		this.costoDeEntrada = costo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public Money getCostoDeEntrada() {
		return this.costoDeEntrada;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + costoDeEntrada.hashCode();
		result = prime * result + nombre.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atraccion other = (Atraccion) obj;
		if (!costoDeEntrada.equals(other.costoDeEntrada))
			return false;
		if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

}
