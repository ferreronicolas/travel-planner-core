package planificadores;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

class PlanificadorFinder {

    private static final Logger LOG = 
            Logger.getLogger(PlanificadorFinder.class.getName());
    private static final String CLASS_SUFFIX = ".class";
    private static final String JAR_SUFFIX   = ".jar";

    protected final String dirName;
    private File   dirFile;
    private URL[]  jarsEnDirFile;
    private Map<String, String> traducciones;

    public PlanificadorFinder(String dirName) {
        checkNull(dirName);
        LOG.log(Level.INFO, "Creando finder. dirName = " + dirName);
        this.dirName = dirName;
        buscarDirEnClasspath();
        LOG.log(Level.INFO, dirName + " encontrado en classpath.");
        checkDirFile();
        buscarDireccionesDeJars();
        traducciones = new HashMap<String, String>();
    }

    protected void checkNull(String dirName) {
        if (dirName == null) {
            throw new NullPointerException("dirName no puede ser null.");
        }
    }

    protected void buscarDirEnClasspath() {
        LOG.log(Level.INFO, "Buscando " + dirName + " en classpath.");
        String   classpath   = System.getProperty("java.class.path");
        String[] dirs = classpath.split(System.getProperty("path.separator"));
        dirFile = Arrays.stream(dirs)
                     .map(dir -> new File(dir))
                     .filter(dir -> dir.getName().equals(dirName))
                     .findFirst()
                     .orElseThrow(() -> 
                        new IllegalArgumentException("El directorio " + dirName 
                                                + " no existe en classpath."));
    }
    
    protected void checkDirFile() {
        if (!dirFile.isDirectory()) {
            throw new IllegalArgumentException(dirName 
                    + " no es un directorio.");
        }
    }
    
    @SuppressWarnings("resource")
    protected void buscarDireccionesDeJars() {
        File[] jars = dirFile.listFiles(f -> f.getName().endsWith(JAR_SUFFIX));
        List<URL> urls = new LinkedList<>();
        for (File jar : jars) {
            try {
                new JarFile(jar);
                urls.add(jar.toURI().toURL());
            } catch (IOException ex) {
                LOG.log(Level.FINE, jar.getName() + " no es un Jar válido.");
            }
        }
        jarsEnDirFile = urls.toArray(new URL[0]);
    }

    public Collection<String> listarPlanificadores() {
        List<String> clasesEncontradas = new LinkedList<>();
        List<JarFile> jars = buscarJarsEnDirectorio(dirFile);
        jars.forEach(jar -> clasesEncontradas.addAll(buscarClasesEnJar(jar)));
        URLClassLoader cl = new URLClassLoader(jarsEnDirFile);
        Class<?> clazz;
        Iterator<String> it = clasesEncontradas.iterator();
        while (it.hasNext()) {
            String binaryName = it.next();
            try {
                clazz = Class.forName(binaryName, false, cl);
                if (PlanificadorAbstracto.class.isAssignableFrom(clazz)) {
                    PlanificadorAbstracto planificador = (PlanificadorAbstracto) clazz.newInstance();
                    traducciones.put(planificador.getNombre(), binaryName);
                }
            } catch (Exception e) {
                LOG.log(Level.WARNING,
                        "Error al listar planificadores: " + e.getMessage());
            }
        }
        return new HashSet<String>(traducciones.keySet());
    }

    List<JarFile> buscarJarsEnDirectorio(File dir) {
        if (dir == null) {
            throw new NullPointerException("dir no puede ser null.");
        }
        if (!dir.isDirectory()) {
            throw new IllegalArgumentException("dir no es un directorio.");
        }
        File[] jarFiles = dir.listFiles(f -> f.getName().endsWith(JAR_SUFFIX));
        if (jarFiles.length == 0) {
            return new LinkedList<>();
        }
        List<JarFile> ret = new LinkedList<>();
        for (File file : jarFiles) {
            try {
                ret.add(new JarFile(file));
            } catch (IOException ex) {
                LOG.log(Level.INFO, file.getName() + " no es un jar.", ex);
            }
        }
        return ret;
    }

    List<String> buscarClasesEnJar(JarFile jar) {
        if (jar == null) {
            throw new NullPointerException("Jar no puede ser null.");
        }
        return jar.stream()
                .filter(je -> je.getName().endsWith(CLASS_SUFFIX))
                .map(je -> je.getName())
                .map(nm -> nm.substring(0, nm.length() - CLASS_SUFFIX.length()))
                .map(nm -> nm.replace(File.separator, ".") )
                .collect(Collectors.toList());
    }

    public URL[] getUrls() {
        return this.jarsEnDirFile.clone();
    }
    
    public String getDirName() {
        return this.dirName;
    }

    public String traducirNombre(String nombrePlanificador) {
        return traducciones.get(nombrePlanificador);
    }
}

