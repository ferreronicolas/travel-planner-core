package planificadores;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import domain.Mapa;

public class PlanificadorNoEncontrado extends PlanificadorAbstracto {

    private String nombre = "Planificador no encontrado";

    @Override
    public String getNombre() {
        return nombre;
    }

    @Override
    public void planificarViaje(Mapa mapa, Collection<String> ciudadesElegidas, String inicio) {}

    @Override
    public List<String> getItinerario() {
        return new ArrayList<String>();
    }

    @Override
    public String elegirAlojamiento(String ciudad) {
        return "";
    }

    @Override
    public String elegirAtraccion(String ciudad) {
        return "";
    }

    @Override
    public String elegirRuta(String ciudad1, String ciudad2) {
        return "";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + nombre.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PlanificadorNoEncontrado other = (PlanificadorNoEncontrado) obj;
        if (!nombre.equals(other.nombre))
            return false;
        return true;
    }

}
