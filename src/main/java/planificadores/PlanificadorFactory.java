package planificadores;

import java.net.URLClassLoader;

class PlanificadorFactory {

    public static PlanificadorAbstracto getPlanificador(String clase) {
        if (clase == null) {
            throw new NullPointerException("Clase no puede ser null");
        }
        ClassLoader cl = new URLClassLoader(PlanificadorAbstracto.getFinder().getUrls());
        Class<?> clazz;
        try {
            clazz = Class.forName(clase, true, cl);
            if (!PlanificadorAbstracto.class.isAssignableFrom(clazz)) {
                return new PlanificadorNoEncontrado();
            }
            return (PlanificadorAbstracto) clazz.newInstance();
        } catch (Exception e) {
            return new PlanificadorNoEncontrado();
        }
    }
}

