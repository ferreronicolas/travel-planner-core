package planificadores;

import java.util.Collection;
import java.util.List;

import domain.Mapa;

public abstract class PlanificadorAbstracto {

    protected static PlanificadorAbstracto INSTANCIA;
    protected static PlanificadorFinder    FINDER;        
    
    static {
        INSTANCIA = new PlanificadorNoEncontrado();
        cargarFinder();
    }
    
    public static PlanificadorAbstracto getInstancia() {
        return INSTANCIA;
    }

    static void cargarFinder() {
        try {
            FINDER = new PlanificadorFinder(System.getProperty("dir.planificadores"));
        } catch (Exception e) {
            FINDER = new FinderCarpetaNoEncontrada();
        }
    }
    
    static PlanificadorFinder getFinder() {
        return FINDER;
    }

    public static PlanificadorAbstracto elegirPlanificador(String planificador) {
        String binaryName = FINDER.traducirNombre(planificador);
        INSTANCIA = PlanificadorFactory.getPlanificador(binaryName);
        return INSTANCIA;
    }

    public static Collection<String> listarPlanificadores() {
        return FINDER.listarPlanificadores();        
    }

    public abstract void planificarViaje(Mapa mapa, Collection<String> ciudadesElegidas, String ciudadInicial);
    public abstract String getNombre();
    public abstract List<String> getItinerario();
    public abstract String elegirAlojamiento(String ciudad);
    public abstract String elegirAtraccion(String ciudad);
    public abstract String elegirRuta(String ciudad1, String ciudad2);
}
