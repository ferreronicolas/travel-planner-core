package planificadores;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

class FinderCarpetaNoEncontrada extends PlanificadorFinder {

    public FinderCarpetaNoEncontrada() {
        super("Carpeta no encontrada");
    }

    @Override
    protected void checkNull(String dirName) {
        
    }

    @Override
    protected void buscarDirEnClasspath() {
        
    }
    
    @Override
    protected void checkDirFile() {

    }

    @Override
    protected void buscarDireccionesDeJars() {
        
    }

    @Override
    public Collection<String> listarPlanificadores() {
        return new ArrayList<String>();
    }
    @Override
    public URL[] getUrls() {
        return new URL[0];
    }
    
    @Override
    public String traducirNombre(String nombrePlanificador) {
        return "";
    }
}
