package userInterface;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import domain.Mapa;
import planificadores.PlanificadorAbstracto;

public class TravelPlanner {

    private Collection<Observer> observadores;

    public TravelPlanner() {
        observadores = new HashSet<Observer>();
    }

    public void agregarObservador(Observer observer) {
        checkNull(observer);
        observadores.add(observer);
    }

    public void removerObservador(Observer observer) {
        checkNull(observer);
        observadores.remove(observer);
    }
    
    private void checkNull(Observer observer) {
        if (observer == null) {
            throw new NullPointerException("Observer no puede ser null.");
        }
    }

    public void notificarObservadores() {
        observadores.forEach(Observer::update);
    }

    public Collection<String> getCiudades() {
        Mapa mapa = Mapa.getMapa();
        return mapa.getNombresDeCiudades();
    }

    public Collection<String> getPlanificadores() {
        return PlanificadorAbstracto.listarPlanificadores();
    }

    public Collection<String> getAtracciones(String ciudad) {
        Mapa mapa = Mapa.getMapa();
        return mapa.getCiudad(ciudad).getNombresDeAtracciones();
    }

    public Collection<String> getAlojamientos(String ciudad) {
        Mapa mapa = Mapa.getMapa();
        return mapa.getCiudad(ciudad).getNombresDeAlojamientos();
    }

    public String getPrecioAlojamiento(String ciudad, String alojamiento) {
        Mapa mapa = Mapa.getMapa();
        return mapa.getCiudad(ciudad)
                   .getAlojamiento(alojamiento).getCostoPorDia().toString();
    }

    public String getPrecioAtraccion(String ciudad, String atraccion) {
        Mapa mapa = Mapa.getMapa();
        return mapa.getCiudad(ciudad)
                   .getAtraccion(atraccion).getCostoDeEntrada().toString();
    }

    public void planificar(Collection<String> ciudades, String planificador, String ciudadInicial) {
        Mapa mapa = Mapa.getMapa();
        PlanificadorAbstracto.elegirPlanificador(planificador).planificarViaje(mapa, ciudades, ciudadInicial);
        notificarObservadores();
    }

    public List<String> getItinerario() {
        return PlanificadorAbstracto.getInstancia().getItinerario();
    }
    
    public String elegirAlojamiento(String ciudad) {
        return PlanificadorAbstracto.getInstancia().elegirAlojamiento(ciudad);
    }

    public String elegirAtraccion(String ciudad) {
        return PlanificadorAbstracto.getInstancia().elegirAlojamiento(ciudad);
    }

    public String elegirRuta(String ciudad1, String ciudad2) {
        return PlanificadorAbstracto.getInstancia().elegirRuta(ciudad1, ciudad2);
    }
}
