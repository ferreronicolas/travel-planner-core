package userInterface;

public interface Observer {

	public void update();
}
