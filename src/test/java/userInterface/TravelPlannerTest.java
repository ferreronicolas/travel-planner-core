package userInterface;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import domain.Alojamiento;
import domain.Atraccion;
import domain.Ciudad;
import domain.Mapa;
import domain.Money;
import domain.Ruta;

public class TravelPlannerTest {

    @Test(expected = NullPointerException.class)
    public void agregarObservadorNullTest() {
        TravelPlanner tv = new TravelPlanner();
        tv.agregarObservador(null);
    }
    
    @Test(expected = NullPointerException.class)
    public void removerObservadorNullTest() {
        TravelPlanner tv = new TravelPlanner();
        tv.removerObservador(null);
    }
    
    @Test
    public void notificarObservadorTest() {
        TravelPlanner tv = new TravelPlanner();
        ObserverStub os1 = new ObserverStub();
        ObserverStub os2 = new ObserverStub();
        tv.agregarObservador(os1);
        tv.agregarObservador(os2);
        tv.notificarObservadores();
        assertEquals(1, os1.getCalls());
        assertEquals(1, os2.getCalls());
        tv.removerObservador(os2);
        tv.notificarObservadores();
        assertEquals(2, os1.getCalls());
        assertEquals(1, os2.getCalls());
    }

    @Test
    public void getCiudadesTest() {
        Mapa mapa = Mapa.getMapa();
        TravelPlanner tv = new TravelPlanner();
        assertEquals(mapa.getNombresDeCiudades(), tv.getCiudades());
        Ciudad ciudad = new Ciudad("BsAs");
        mapa.addCiudad(ciudad);
        assertEquals(1, tv.getCiudades().size());
        Collection<String> ciudades = tv.getCiudades();
        ciudades.forEach(nombre -> assertEquals("BsAs", nombre));
        mapa.removeCiudad(ciudad);
    }

    @Test
    public void getPlanificadoresTest() {
        TravelPlanner tv = new TravelPlanner();
        assertEquals(0, tv.getPlanificadores().size());
    }

    @Test
    public void getAtraccionesTest() {
        Mapa mapa = Mapa.getMapa();
        TravelPlanner tv = new TravelPlanner();
        Ciudad ciudad = new Ciudad("BsAs");
        Atraccion atraccion1 = new Atraccion("atraccion 1", Money.CERO);
        Atraccion atraccion2 = new Atraccion("atraccion 2", Money.CERO);
        mapa.addCiudad(ciudad);
        ciudad.addAtraccion(atraccion1);
        ciudad.addAtraccion(atraccion2);
        Collection<String> atracciones = tv.getAtracciones(ciudad.getNombre());
        assertEquals(2, atracciones.size());
        assertTrue(atracciones.contains(atraccion1.getNombre()));
        assertTrue(atracciones.contains(atraccion2.getNombre()));
        mapa.removeCiudad(ciudad);
    }

    @Test
    public void getAlojamientosTest() {
        Mapa mapa = Mapa.getMapa();
        TravelPlanner tv = new TravelPlanner();
        Ciudad ciudad = new Ciudad("BsAs");
        Alojamiento alojamiento1 = new Alojamiento("alojamiento 1", Money.CERO);
        Alojamiento alojamiento2 = new Alojamiento("alojamiento 2", Money.CERO);
        mapa.addCiudad(ciudad);
        ciudad.addAlojamiento(alojamiento1);
        ciudad.addAlojamiento(alojamiento2);
        Collection<String> alojamientos = tv.getAlojamientos(ciudad.getNombre());
        assertEquals(2, alojamientos.size());
        assertTrue(alojamientos.contains(alojamiento1.getNombre()));
        assertTrue(alojamientos.contains(alojamiento2.getNombre()));
        mapa.removeCiudad(ciudad);
    }

    @Test
    public void getPrecioAlojamientoTest() {
        Mapa mapa = Mapa.getMapa();
        TravelPlanner tv = new TravelPlanner();
        Ciudad ciudad = new Ciudad("BsAs");
        Alojamiento alojamiento1 = new Alojamiento("alojamiento 1", new Money("10"));
        Alojamiento alojamiento2 = new Alojamiento("alojamiento 2", new Money("12"));
        mapa.addCiudad(ciudad);
        ciudad.addAlojamiento(alojamiento1);
        ciudad.addAlojamiento(alojamiento2);
        assertEquals(new Money("10").toString(), tv.getPrecioAlojamiento(ciudad.getNombre(), alojamiento1.getNombre()));
        assertEquals(new Money("12").toString(), tv.getPrecioAlojamiento(ciudad.getNombre(), alojamiento2.getNombre()));
        mapa.removeCiudad(ciudad);
    }

    @Test
    public void getPrecioAtraccion() {
        Mapa mapa = Mapa.getMapa();
        TravelPlanner tv = new TravelPlanner();
        Ciudad ciudad = new Ciudad("BsAs");
        Atraccion atraccion1 = new Atraccion("atraccion 1", new Money("9"));
        Atraccion atraccion2 = new Atraccion("atraccion 2", new Money("17"));
        mapa.addCiudad(ciudad);
        ciudad.addAtraccion(atraccion1);
        ciudad.addAtraccion(atraccion2);
        assertEquals(new Money("9").toString(), tv.getPrecioAtraccion(ciudad.getNombre(), atraccion1.getNombre()));
        assertEquals(new Money("17").toString(), tv.getPrecioAtraccion(ciudad.getNombre(), atraccion2.getNombre()));
        mapa.removeCiudad(ciudad);
    }

    @Test
    public void planificar_planificadorNoEncontrado_Test() {
        Mapa mapa = Mapa.getMapa();
        ObserverStub os = new ObserverStub();
        TravelPlanner tv = new TravelPlanner();
        tv.agregarObservador(os);
        Ciudad ciudad1 = new Ciudad("BsAs");
        Ciudad ciudad2 = new Ciudad("San Miguel");
        Ruta ruta = new Ruta(ciudad1, ciudad2, 5);
        mapa.addCiudad(ciudad1);
        mapa.addCiudad(ciudad2);
        mapa.addRuta(ruta);
        Collection<String> ciudadesSeleccionadas = new ArrayList<String>();
        ciudadesSeleccionadas.add(ciudad1.getNombre());
        ciudadesSeleccionadas.add(ciudad2.getNombre());
        tv.planificar(ciudadesSeleccionadas, "planificador", ciudad1.getNombre());
        assertEquals(1, os.getCalls());
        assertEquals(0, tv.getItinerario().size());
        assertTrue("".equals(tv.elegirAlojamiento(ciudad1.getNombre())));
        assertTrue("".equals(tv.elegirAlojamiento(ciudad2.getNombre())));
        assertTrue("".equals(tv.elegirAtraccion(ciudad1.getNombre())));
        assertTrue("".equals(tv.elegirAtraccion(ciudad2.getNombre())));
        assertTrue("".equals(tv.elegirRuta(ciudad1.getNombre(), ciudad2.getNombre())));
        mapa.removeCiudad(ciudad1);
        mapa.removeCiudad(ciudad2);
    }
}
