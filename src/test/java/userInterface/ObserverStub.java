package userInterface;

public class ObserverStub implements Observer {
	
	private int calls;
	
	public ObserverStub() {
		this.calls = 0;
	}
	
	public int getCalls() {
		return this.calls;
	}

	@Override
	public void update() {
		++this.calls;
	}

}
