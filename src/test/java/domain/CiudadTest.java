package domain;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class CiudadTest {
	
    Alojamiento alojamiento;
    Atraccion   atraccion;
	String nombre;
	
	@Before
	public void setUp() {
	    alojamiento = new Alojamiento("alojamiento", new Money("0"));
	    atraccion = new Atraccion("atraccion", new Money("0"));
		nombre = "Ciudad";
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_constructorNombreNull_expect_exception() {
		@SuppressWarnings("unused")
		Ciudad ciudad = new Ciudad(null);
	}
	
	@Test
	public void when_getNombre_expect_nombre() {
		Ciudad ciudad = new Ciudad(nombre);
		assertEquals(nombre, ciudad.getNombre());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_addAlojamientoNull_expect_exception() {
		Ciudad ciudad = new Ciudad(nombre);
		ciudad.addAlojamiento(null);
	}
	
	@Test
	public void when_getAlojamientosSinAlojamientos_expect_setVacio() {
		Ciudad ciudad = new Ciudad(nombre);
		assertEquals(0, ciudad.getAlojamientos().size());
	}
	
	@Test
	public void when_getAlojamientosConUnAlojamiento_expect_setConUnAlojamiento() {
		Ciudad ciudad = new Ciudad(nombre);
		ciudad.addAlojamiento(alojamiento);
		assertEquals(1, ciudad.getAlojamientos().size());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_removeAlojamientoNull_expect_exception() {
		Ciudad ciudad = new Ciudad(nombre);
		ciudad.removeAlojamiento(null);
	}
	
	@Test
	public void when_removeAlojamientoEnCiudadConUnAlojamiento_expect_setAlojamientosVacio() {
		Ciudad ciudad = new Ciudad(nombre);
		ciudad.addAlojamiento(alojamiento);
		ciudad.removeAlojamiento(alojamiento);
		assertEquals(0, ciudad.getAlojamientos().size());
	}
	
	@Test
	public void when_removeAlojamientoQueNoEstaEnCiudad_expect_ejecucionNormal() {
		Ciudad ciudad = new Ciudad(nombre);
		ciudad.removeAlojamiento(alojamiento);
		assertEquals(0, ciudad.getAlojamientos().size());
	}
	
	@Test
	public void when_getAlojamientos_expect_setCopia() {
		Ciudad ciudad = new Ciudad(nombre);
		ciudad.addAlojamiento(alojamiento);
		Set<Alojamiento> alojamientos = ciudad.getAlojamientos();
		alojamientos.remove(alojamiento);
		assertEquals(1, ciudad.getAlojamientos().size());
	}
	
	@Test
	public void when_getAlojamientoPorNombre_expect_alojamiento() {
	    Ciudad ciudad = new Ciudad(nombre);
	    ciudad.addAlojamiento(alojamiento);
	    assertTrue(alojamiento == ciudad.getAlojamiento(alojamiento.getNombre()));
	}
	
	@Test
	public void when_getNombresDeAlojamientos_expect_coleccionDeNombres() {
	    Ciudad ciudad = new Ciudad(nombre);
	    ciudad.addAlojamiento(alojamiento);
	    ciudad.addAlojamiento(new Alojamiento("alojamiento2", new Money("0")));
	    Set<String> alojamientos = new HashSet<String>(2);
	    alojamientos.add(alojamiento.getNombre());
	    alojamientos.add("alojamiento2");
	    assertEquals(alojamientos, ciudad.getNombresDeAlojamientos());
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Test(expected = IllegalArgumentException.class)
	public void when_addAtraccionNull_expect_exception() {
		Ciudad ciudad = new Ciudad(nombre);
		ciudad.addAtraccion(null);
	}
	
	@Test
	public void when_getAtraccionesSinAtracciones_expect_setVacio() {
		Ciudad ciudad = new Ciudad(nombre);
		assertEquals(0, ciudad.getAtracciones().size());
	}
	
	@Test
	public void when_getAtraccionesConUnaAtraccion_expect_setConUnaAtraccion() {
		Ciudad ciudad = new Ciudad(nombre);
		ciudad.addAtraccion(atraccion);
		assertEquals(1, ciudad.getAtracciones().size());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_removeAtraccionNull_expect_exception() {
		Ciudad ciudad = new Ciudad(nombre);
		ciudad.removeAtraccion(null);
	}
	
	@Test
	public void when_removeAtraccionEnCiudadConUnaAtraccion_expect_setAtraccionesVacio() {
		Ciudad ciudad = new Ciudad(nombre);
		ciudad.addAtraccion(atraccion);
		ciudad.removeAtraccion(atraccion);
		assertEquals(0, ciudad.getAtracciones().size());
	}
	
	@Test
	public void when_removeAtraccionQueNoEstaEnCiudad_expect_ejecucionNormal() {
		Ciudad ciudad = new Ciudad(nombre);
		ciudad.removeAtraccion(atraccion);
		assertEquals(0, ciudad.getAtracciones().size());
	}
	
	
	
	@Test
	public void when_getAtracciones_expect_setCopia() {
		Ciudad ciudad = new Ciudad(nombre);
		ciudad.addAtraccion(atraccion);
		Set<Atraccion> atracciones = ciudad.getAtracciones();
		atracciones.remove(atraccion);
		assertEquals(1, ciudad.getAtracciones().size());
	}
	
	@Test
    public void when_getAtraccionPorNombre_expect_atraccion() {
        Ciudad ciudad = new Ciudad(nombre);
        ciudad.addAtraccion(atraccion);
        assertTrue(atraccion == ciudad.getAtraccion(atraccion.getNombre()));
    }
    
    @Test
    public void when_getNombresDeAtracciones_expect_coleccionDeAtracciones() {
        Ciudad ciudad = new Ciudad(nombre);
        ciudad.addAtraccion(atraccion);
        ciudad.addAtraccion(new Atraccion("atraccion2", new Money("0")));
        Set<String> atracciones = new HashSet<String>(2);
        atracciones.add(atraccion.getNombre());
        atracciones.add("atraccion2");
        assertEquals(atracciones, ciudad.getNombresDeAtracciones());
    }
    
    
	
	@Test
	public void when_equalsMismoObj_expect_equalsTrue() {
		Ciudad ciudad = new Ciudad(nombre);
		assertTrue(ciudad.equals(ciudad));
	}
	
	@Test
	public void when_equalsNull_expect_equalsFalse() {
		Ciudad ciudad = new Ciudad(nombre);
		assertFalse(ciudad.equals(null));
	}
	
	@Test
	public void when_equalsDiferenteClase_expect_equalstFalse() {
		Ciudad ciudad = new Ciudad(nombre);
		assertFalse(ciudad.equals(new CiudadTest()));
	}
	
	@Test
	public void when_equalsConAlojamientosDistintos_expect_equalsFalse() {
		Ciudad ciudad1 = new Ciudad("Ciudad 1");
		Ciudad ciudad2 = new Ciudad("Ciudad 2");
		ciudad1.addAlojamiento(alojamiento);
		assertFalse(ciudad1.equals(ciudad2));
	}
	
	@Test
	public void when_equalsConAtraccionesDistintos_expect_equalsFalse() {
		Ciudad ciudad1 = new Ciudad("Ciudad 1");
		Ciudad ciudad2 = new Ciudad("Ciudad 2");
		ciudad1.addAtraccion(atraccion);
		assertFalse(ciudad1.equals(ciudad2));
	}
	
	@Test
	public void when_equalsConNombreDiferente_expect_equalsFalse() {
		Ciudad ciudad1 = new Ciudad(nombre);
		Ciudad ciudad2 = new Ciudad(nombre + "2");
		assertFalse(ciudad1.equals(ciudad2));
	}
	
	@Test
	public void when_equalsConObjetosDistintosPeroIgualesAtributos_expect_equalsTrue() {
		Ciudad ciudad1 = new Ciudad(nombre);
		Ciudad ciudad2 = new Ciudad(nombre);
		assertTrue(ciudad1.equals(ciudad2));
	}

	@Test
	public void hashcodeTest() {
	    Ciudad ciudad1 = new Ciudad(nombre);
	    Ciudad ciudad2 = new Ciudad("Ciudad");
	    assertTrue(ciudad1.hashCode() == ciudad2.hashCode());
	}
}
