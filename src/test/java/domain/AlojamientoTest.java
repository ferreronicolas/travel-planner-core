package domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class AlojamientoTest {

	@Test(expected = IllegalArgumentException.class)
	public void when_construirConNombreNull_expect_exception() {
		Money costo = new Money("2");
		@SuppressWarnings("unused")
		Alojamiento a = new Alojamiento(null, costo);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_construirConMoneyNull_expect_exception() {
		@SuppressWarnings("unused")
		Alojamiento a = new Alojamiento("Alojamiento", null);
	}
	
	@Test
	public void when_getNombre_expect_nombre() {
		String nombre = "Alojamiento";
		Alojamiento a = new Alojamiento(nombre, new Money("2"));
		assertEquals(nombre, a.getNombre());
	}
	
	@Test
	public void when_getCostoPorDia_expect_money() {
		String nombre = "Alojamiento";
		Money costo  = new Money("2");
		Alojamiento a = new Alojamiento(nombre, costo);
		assertEquals(costo, a.getCostoPorDia());
	}
	
	@Test
	public void when_dosAlojamientosTienenMismosAtributos_expect_equalHash() {
		String nombre1 = "Alojamiento";
		String nombre2 = "Alojamiento";
		Money costo1  = new Money("1");
		Money costo2  = new Money("1");
		Alojamiento a1 = new Alojamiento(nombre1, costo1);
		Alojamiento a2 = new Alojamiento(nombre2, costo2);
		assertTrue(a1.hashCode() == a2.hashCode());
	}
	
	@Test
	public void when_dosAlojamientosTienenMismosAtributos_expect_equalsTrue() {
		String nombre1 = "Alojamiento";
		String nombre2 = "Alojamiento";
		Money costo1  = new Money("1");
		Money costo2  = new Money("1");
		Alojamiento a1 = new Alojamiento(nombre1, costo1);
		Alojamiento a2 = new Alojamiento(nombre2, costo2);
		assertTrue(a1.equals(a2));
	}
	
	@Test
	public void when_alojamientoEqualsMismoObjeto_expect_equalsTrue() {
		String nombre = "Alojamiento";
		Money costo  = new Money("4");
		Alojamiento a = new Alojamiento(nombre, costo);
		assertTrue(a.equals(a));
	}
	
	@Test
	public void when_alojamientoEqualsNull_expect_equalsFalse() {
		String nombre = "Alojamiento";
		Money costo  = new Money("4");
		Alojamiento a = new Alojamiento(nombre, costo);
		assertFalse(a.equals(null));
	}
	
	@Test
	public void when_alojamientoEqualsInstanceOtraClase_expect_equalsFalse() {
		String nombre = "Alojamiento";
		Money costo  = new Money("4");
		Alojamiento a = new Alojamiento(nombre, costo);
		assertFalse(a.equals(new String("")));
	}
	
	@Test
	public void when_alojamientosDistintoCostoPorDiaIgualNombre_expect_equalsFalse() {
		String nombre1 = "Alojamiento";
		String nombre2 = "Alojamiento";
		Money costo1  = new Money("1");
		Money costo2  = new Money("2");
		Alojamiento a1 = new Alojamiento(nombre1, costo1);
		Alojamiento a2 = new Alojamiento(nombre2, costo2);
		assertFalse(a1.equals(a2));
	}
	
	@Test
	public void when_alojamientosDistintoNombreIgualCostoPorDia_expect_equalsFalse() {
		String nombre1 = "Alojamiento";
		String nombre2 = "nombre";
		Money costo1  = new Money("1");
		Money costo2  = new Money("1");
		Alojamiento a1 = new Alojamiento(nombre1, costo1);
		Alojamiento a2 = new Alojamiento(nombre2, costo2);
		assertFalse(a1.equals(a2));
	}

	@Test
	public void alojamientoNulo_test() {
	    assertEquals(Alojamiento.getAlojamientoNulo(), Alojamiento.getAlojamientoNulo());
	}
}
