package domain;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class MapaTest {

	private Map<String, Ciudad> ciudades1;
	private Map<String, Ciudad> ciudades2;
	private Map<String, Ciudad> ciudades3;
	private Set<Ruta> rutas1;
	private Set<Ruta> rutas2;
	private Ciudad ciudad1;
	private Ciudad ciudad2;
	private Ciudad ciudad3;
	private Ciudad ciudadAislada;
	private Ruta ruta1;
	private Ruta ruta2;
	private Mapa mapaCiudades;
	private Mapa mapaLleno;
	private Mapa mapaVacio;
	private Ciudad n;
	private Ruta a;
	
	@Before
	public void setUp() {
		ciudades1 = new HashMap<String, Ciudad>();
		ciudades2 = new HashMap<String, Ciudad>();
		ciudades3 = new HashMap<String, Ciudad>();
		rutas1 = new HashSet<Ruta>();
		rutas2 = new HashSet<Ruta>();
		ciudad1 = new Ciudad("Ciudad1");
		ciudad2 = new Ciudad("Ciudad2");
		ciudad3 = new Ciudad("Ciudad3");
		ciudadAislada = new Ciudad("CiudadAislada");
		ruta1 = new Ruta(ciudad1, ciudad2, 4);
		ruta2 = new Ruta(ciudad2, ciudad3, 3);
		this.ciudades1.put(ciudad1.getNombre(), ciudad1);
		this.ciudades1.put(ciudad2.getNombre(), ciudad2);
		mapaCiudades = new Mapa(ciudades1);
		this.ciudades2.put(ciudad1.getNombre(), ciudad1);
		this.ciudades2.put(ciudad2.getNombre(), ciudad2);
		this.ciudades2.put(ciudad3.getNombre(), ciudad3);
		this.ciudades2.put(ciudadAislada.getNombre(), ciudadAislada);
		this.rutas1.add(ruta1);
		this.rutas1.add(ruta2);
		mapaLleno = new Mapa(ciudades2, rutas1);
		mapaVacio = new Mapa(ciudades3, rutas2);
		
		n = new Ciudad("n");
		a = new Ruta(this.ciudad3, this.ciudadAislada, 4);
	}

	@Test(expected = IllegalArgumentException.class)
	public void ciudadesNull_expected_exception() {
		@SuppressWarnings("unused")
		Mapa m = new Mapa(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void rutasNull_expected_exception() {
		@SuppressWarnings("unused")
		Mapa m = new Mapa(ciudades1, null);
	}
	
	@Test
	public void testCantCiudades() {
		assertEquals(2, this.mapaCiudades.cantCiudades());
		assertEquals(4, this.mapaLleno.cantCiudades());
		assertEquals(0, Mapa.getMapa().cantCiudades());
	}

	@Test
	public void testSize() {
		assertEquals(2, this.mapaLleno.cantRutas());
		assertEquals(0, this.mapaVacio.cantRutas());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void cantVecinosNull_expected_exception() {
		this.mapaLleno.cantVecinos(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void cantVecinosCiudadFuraDelMapa_expected_exception() {
		this.mapaLleno.cantVecinos(n);
	}

	@Test
	public void testCantVecinos() {
		assertEquals(2, this.mapaLleno.cantVecinos(this.ciudad2));
		assertEquals(1, this.mapaLleno.cantVecinos(this.ciudad1));
		assertEquals(0, this.mapaLleno.cantVecinos(this.ciudadAislada));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void addAristaNull_expected_exception() {
		this.mapaLleno.addRuta(null);
	}
	
	@Test
	public void testAddArista() {
		this.mapaLleno.addRuta(a);
		assertEquals(3, this.mapaLleno.cantRutas());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void removeAristaNull_expected_exception() {
		this.mapaLleno.removeRuta(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void removeAristaFueraDelMapa_expected_exception() {
		this.mapaLleno.removeRuta(a);
	}
	
	@Test
	public void testRemoveArista() {
		this.mapaLleno.removeRuta(ruta1);
		assertEquals(1, this.mapaLleno.cantRutas());
		this.mapaLleno.removeRuta(ruta2);
		assertEquals(0, this.mapaLleno.cantRutas());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void addCiudadNull_expected_exception() {
		this.mapaLleno.addCiudad(null);
	}

	@Test
	public void testAddCiudad() {
		this.mapaLleno.addCiudad(n);
		assertEquals(5, this.mapaLleno.cantCiudades());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void removeCiudadNull_expected_exception() {
		this.mapaLleno.removeCiudad(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void removeCiudadFuraDelMapa_expected_exception() {
		this.mapaLleno.cantVecinos(n);
	}

	@Test
	public void testRemoveCiudad() {
		this.mapaLleno.removeCiudad(ciudadAislada);
		assertEquals(3, this.mapaLleno.cantCiudades());
		assertEquals(2, this.mapaLleno.cantRutas());
		this.mapaLleno.removeCiudad(ciudad2);
		assertEquals(2, this.mapaLleno.cantCiudades());
		assertEquals(0, this.mapaLleno.cantRutas());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void vecinosNull_expected_exception() {
		this.mapaLleno.getVecinos(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void vecinosFueraDelMapa_expected_exception() {
		this.mapaLleno.getVecinos(n);
	}
	
	@Test
	public void testVecinos() {
		Set<Ciudad> vecinos = new HashSet<Ciudad>();
		vecinos = mapaLleno.getVecinos(ciudad2);
		assertEquals(2, vecinos.size());
		assertEquals(false, vecinos.contains(this.ciudad2));
		assertEquals(false, vecinos.contains(this.ciudadAislada));
		assertEquals(true, vecinos.contains(this.ciudad3));
		assertEquals(true, vecinos.contains(this.ciudad1));
	}

	@Test
	public void testAristas() {
		Set<Ruta> aristas;
		aristas = this.mapaLleno.rutas();
		assertEquals(true, aristas.contains(ruta1));
		assertEquals(false, aristas.contains(a));
	}

	@Test
	public void testCiudades() {
		Set<Ciudad> ciudades;
		ciudades = this.mapaLleno.ciudades();
		assertEquals(true, ciudades.contains(this.ciudad1));
		assertEquals(true, ciudades.contains(this.ciudad2));
		ciudades = this.mapaCiudades.ciudades();
		assertEquals(false, ciudades.contains(this.ciudadAislada));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void getCiudadNull_expected_exception() {
		this.mapaLleno.getCiudad(null);
	}
	
	@Test
	public void testGetCiudad() {
		assertEquals(this.ciudad1, this.mapaLleno.getCiudad("Ciudad1"));
		assertEquals(null, this.mapaVacio.getCiudad("Ciudad1"));
	}

	@Test
	public void testNombresDeCiudades() {
	    Set<String> nombresEsperados = new HashSet<String>(4);
	    nombresEsperados.add(ciudad1.getNombre());
	    nombresEsperados.add(ciudad2.getNombre());
	    nombresEsperados.add(ciudad3.getNombre());
	    nombresEsperados.add(ciudadAislada.getNombre());
	    Collection<String> nombresDeCiudades = this.mapaLleno.getNombresDeCiudades();
	    assertEquals(nombresEsperados.size(), nombresDeCiudades.size());
	    assertTrue(nombresDeCiudades.containsAll(nombresEsperados));
	}

	@Test
	public void testGetRutasEntre() {
	    Set<Ruta> vacio = mapaLleno.getRutasEntre(ciudad1, ciudadAislada);
	    Set<Ruta> unaRuta = mapaLleno.getRutasEntre(ciudad1, ciudad2);
	    assertEquals(0, vacio.size());
	    assertEquals(1, unaRuta.size());
	    assertTrue(unaRuta.contains(ruta1));
	}

	@Test
	public void testGetRutaMasBarata() {
	    Ruta ret = mapaLleno.getRutaMasBarata(ciudad2, ciudad3);
	    assertNotNull(ret);
	    assertEquals(ruta2, ret);
	    
	    Ciudad c1 = new Ciudad("c1");
	    Ciudad c2 = new Ciudad("c2");
	    Ruta   r1 = new Ruta(c1, c2, 2);
	    Ruta   r2 = new Ruta(c2, c1, 9);
	    r1.setCosto(new Money("10"));
	    r2.setCosto(new Money("9"));
	    Map<String, Ciudad> newMap = new HashMap<String, Ciudad>(2);
	    Set<Ruta> newRutas = new HashSet<Ruta>(2);
	    newMap.put(c1.getNombre(), c1);
	    newMap.put(c2.getNombre(), c2);
	    newRutas.add(r1);
	    newRutas.add(r2);
	    Mapa newMapa = new Mapa(newMap, newRutas);
	    
	    assertNotNull(newMapa.getRutaMasBarata(c1, c2));
	    assertNotNull(newMapa.getRutaMasBarata(c2, c1));
	    assertEquals(r2, newMapa.getRutaMasBarata(c1, c2));
	    assertEquals(r2, newMapa.getRutaMasBarata(c2, c1));
	}

	@Test
	public void testGetVecinosOrdenadosPorCosto() {
	    ruta1.setCosto(new Money("10"));
	    ruta2.setCosto(new Money("11"));
	    List<Ciudad> ret = mapaLleno.getVecinosOrdenados(ciudad2,
	            mapaLleno.getComparatorVecinosCostoMenorAMayor(ciudad2));
	    assertEquals(2, ret.size());
	    assertEquals(ciudad1, ret.get(0));
	    assertEquals(ciudad3, ret.get(1));
	}

	@Test
	public void testGetVecinosOrdenados() {
	    Ciudad c1 = new Ciudad("c1");
        Ciudad c2 = new Ciudad("c2");
        Ciudad c3 = new Ciudad("c3");
        Ciudad c4 = new Ciudad("c4");
        Ciudad c5 = new Ciudad("c5");
        Ruta   r1 = new Ruta(c1, c2, 2);
        Ruta   r2 = new Ruta(c2, c1, 9);
        Ruta   r3 = new Ruta(c1, c2, 6);
        Ruta   r4 = new Ruta(c3, c1, 6);
        Ruta   r5 = new Ruta(c1, c4, 9);
        Ruta   r6 = new Ruta(c1, c4, 10);
        r1.setCosto(new Money("10"));
        r2.setCosto(new Money("7"));
        r3.setCosto(new Money("8"));
        r4.setCosto(new Money("13"));
        r5.setCosto(new Money("8"));
        r6.setCosto(new Money("8"));
        Map<String, Ciudad> newMap = new HashMap<String, Ciudad>(5);
        Set<Ruta> newRutas = new HashSet<Ruta>(6);
        newMap.put(c1.getNombre(), c1);
        newMap.put(c2.getNombre(), c2);
        newMap.put(c3.getNombre(), c3);
        newMap.put(c4.getNombre(), c4);
        newMap.put(c5.getNombre(), c5);
        newRutas.add(r1);
        newRutas.add(r2);
        newRutas.add(r3);
        newRutas.add(r4);
        newRutas.add(r5);
        newRutas.add(r6);
        Mapa newMapa = new Mapa(newMap, newRutas);
	    
	    
        List<Ciudad> ret = newMapa.getVecinosOrdenados(c1, 
                newMapa.getComparatorVecinosCostoMayorAMenor(c1));
        assertEquals(3, ret.size());
        assertEquals(c3, ret.get(0));
        assertEquals(c4, ret.get(1));
        assertEquals(c2, ret.get(2));
	}
}
