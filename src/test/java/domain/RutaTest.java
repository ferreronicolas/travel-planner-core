package domain;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

public class RutaTest {
	
	private static Ciudad ciudad1;
	private static Ciudad ciudad2;
	private static Ciudad ciudad3;
	
	@BeforeClass
	public static void setUpClass() {
		ciudad1 = new Ciudad("Ciudad 1");
		ciudad2 = new Ciudad("Ciudad 2");
		ciudad3 = new Ciudad("Ciudad 3");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_ciudadesSonIguales_expect_exception() {
		@SuppressWarnings("unused")
		Ruta r = new Ruta(ciudad1, ciudad1, 6);
	}

	@Test(expected = IllegalArgumentException.class)
	public void when_extremo1EsNull_expect_exception() {
		@SuppressWarnings("unused")
		Ruta r = new Ruta(null, ciudad1, 1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_extremo2EsNull_expect_exception() {
		@SuppressWarnings("unused")
		Ruta r = new Ruta(ciudad1, null, 1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_distanciaEsCero_expect_exception() {
		@SuppressWarnings("unused")
		Ruta r = new Ruta(ciudad1, ciudad2, 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_distanciaEsNegativo_expect_exception() {
		@SuppressWarnings("unused")
		Ruta r = new Ruta(ciudad1, ciudad2, -1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_extremo1EsNullDistanciaEsDouble_expect_exception() {
		@SuppressWarnings("unused")
		Ruta r = new Ruta(null, ciudad1, Double.valueOf(1.0));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_extremo2EsNullDistanciaEsDouble_expect_exception() {
		@SuppressWarnings("unused")
		Ruta r = new Ruta(ciudad1, null, 1.0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_distanciaEsCeroDouble_expect_exception() {
		@SuppressWarnings("unused")
		Ruta r = new Ruta(ciudad1, ciudad2, Double.valueOf(0.0));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_distanciaEsNegativoDouble_expect_exception() {
		@SuppressWarnings("unused")
		Ruta r = new Ruta(ciudad1, ciudad2, Double.valueOf(-1.5));
	}

	@Test
	public void getNombre_test() {
	    Ruta r = new Ruta(ciudad1, ciudad2, 2);
	    r.setNombre("nombre");
	    assertEquals("nombre", r.getNombre());
	}

	@Test
	public void when_esExtremoConExtremoExistenteEnExtremo1_expect_true() {
	    Ruta r = new Ruta(ciudad1, ciudad2, 1);
	    assertTrue(r.esExtremo(ciudad1));
	}

	@Test
	public void when_esExtremoConExtremoExistenteEnExtremo2_expect_true() {
	    Ruta r = new Ruta(ciudad1, ciudad2, 1);
        assertTrue(r.esExtremo(ciudad2));
	}

	@Test
	public void when_esExtremoConCiudadQueNoEsExtremo_expect_false() {
	    Ruta r = new Ruta(ciudad1, ciudad2, 1);
	    assertFalse(r.esExtremo(ciudad3));
	}

	@Test
	public void when_getExtremos_expect_mismosExtremosQueConstructor() {
	    Ruta r = new Ruta(ciudad1, ciudad3, 2);
	    Set<Ciudad> extremosExpected = new HashSet<Ciudad>(2);
	    extremosExpected.add(ciudad3);
	    extremosExpected.add(ciudad1);
	    assertTrue(r.getExtremos().containsAll(extremosExpected));
	}

	@Test
	public void when_distanciaEs4_expect_distancia() {
		Ruta r = new Ruta(ciudad1, ciudad2, 4);
		assertEquals(new BigDecimal(4), r.getPeso());
	}
	
	@Test
	public void when_extremo1EsCiudad1_expect_ciudad1() {
		Ruta r = new Ruta(ciudad1, ciudad2, 5.2);
		assertEquals(ciudad1, r.getExtremo1());
	}
	
	@Test
	public void when_extremo2Esextremo2_expect_extremo2() {
		Ruta r = new Ruta(ciudad1, ciudad2, 7);
		assertEquals(ciudad2, r.getExtremo2());
	}
	
	@Test
	public void when_getCostoNull_expect_null() {
		Ruta r = new Ruta(ciudad1, ciudad2, 2);
		assertNull(r.getCosto());
	}
	
	@Test
	public void when_setCostoEnConstructorInt_expect_costo() {
		Money ms = new Money("4");
		Ruta r = new Ruta(ciudad1, ciudad2, 4, ms);
		assertEquals(ms, r.getCosto());
	}
	
	@Test
	public void when_setCostoEnConstructorDouble_expect_costo() {
		Money ms = new Money("2");
		Ruta r = new Ruta(ciudad1, ciudad3, 2.3, ms);
		assertEquals(ms, r.getCosto());
	}
	
	@Test
	public void when_setCostoEnMetodo_expect_costo() {
		Money ms = new Money("2");
		Ruta r = new Ruta(ciudad2, ciudad1, 4.5);
		r.setCosto(ms);
		assertEquals(ms, r.getCosto());
	}
	
	@Test
	public void when_rutasConIgualesAtributosVacios_expect_equalHashCode() {
		Ruta r1 = new Ruta(ciudad1, ciudad2, 2);
		Ruta r2 = new Ruta(ciudad1, ciudad2, 2);
		assertTrue(r1.hashCode() == r2.hashCode());
	}
	
	@Test
	public void when_rutasConDistintosAtributos_expect_distintoHashCode() {
		Ruta r1 = new Ruta(ciudad1, ciudad2, 2);
		Ruta r2 = new Ruta(ciudad1, ciudad2, 3);
		r1.setCosto(new Money("2"));
		assertFalse(r1.hashCode() == r2.hashCode());
	}
	
	@Test
	public void when_equalsMismoObjeto_expect_equalsTrue() {
		Ruta r = new Ruta(ciudad1, ciudad2, 2);
		assertTrue(r.equals(r));
	}
	
	@Test
	public void when_equalsConNull_expect_equalsFalse() {
		Ruta r = new Ruta(ciudad1, ciudad2, 3);
		assertFalse(r.equals(null));
	}
	
	@Test
	public void when_equalsConDistintaClase_expect_equalsFalse() {
		Ruta r = new Ruta(ciudad1, ciudad2, 5);
		assertFalse(r.equals(new Money("2")));
	}
	
	@Test
	public void when_equalsConCostoNull_expect_equalsFalse() {
		Ruta r1 = new Ruta(ciudad1, ciudad2, 2);
		Ruta r2 = new Ruta(ciudad1, ciudad2, 2);
		r2.setCosto(new Money("3"));
		assertFalse(r1.equals(r2));
	}
	
	@Test
	public void when_equalsAmbosCostosNull_expect_equalsTrue() {
		Ruta r1 = new Ruta(ciudad1, ciudad2, 2);
		Ruta r2 = new Ruta(ciudad1, ciudad2, 2);
		assertTrue(r1.equals(r2));
	}
	
	@Test
	public void when_equalsConCostoDistintoDeNull_expect_equalsFalse() {
		Ruta r1 = new Ruta(ciudad1, ciudad2, 2);
		Ruta r2 = new Ruta(ciudad1, ciudad2, 2);
		r1.setCosto(new Money("3"));
		assertFalse(r1.equals(r2));
	}
	
	@Test
	public void when_equalsConCostosDistintos_expect_equalsFalse() {
		Ruta r1 = new Ruta(ciudad1, ciudad2, 2);
		Ruta r2 = new Ruta(ciudad1, ciudad2, 2);
		r1.setCosto(new Money("3"));
		r2.setCosto(new Money("2"));
		assertFalse(r1.equals(r2));
	}
	
	@Test
	public void when_extremo1Distintos_expect_equalsFalse() {
		Ruta r1 = new Ruta(ciudad3, ciudad2, 2);
		Ruta r2 = new Ruta(ciudad1, ciudad2, 2);
		r1.setCosto(new Money("3"));
		r2.setCosto(new Money("3"));
		assertFalse(r1.equals(r2));
	}
	
	@Test
	public void when_extremo2Distintos_expect_equalsFalse() {
		Ruta r1 = new Ruta(ciudad1, ciudad2, 2);
		Ruta r2 = new Ruta(ciudad1, ciudad3, 2);
		r1.setCosto(new Money("3"));
		r2.setCosto(new Money("3"));
		assertFalse(r1.equals(r2));
	}
	
	@Test
	public void when_distanciaDistintas_expect_equalsFalse() {
		Ruta r1 = new Ruta(ciudad1, ciudad2, 2);
		Ruta r2 = new Ruta(ciudad1, ciudad2, 3);
		r1.setCosto(new Money("3"));
		r2.setCosto(new Money("3"));
		assertFalse(r1.equals(r2));
	}
	
	@Test
	public void when_rutasConIgualesAtributos_expect_equalsTrue() {
		Ruta r1 = new Ruta(ciudad1, ciudad2, 3);
		Ruta r2 = new Ruta(ciudad1, ciudad2, 3);
		r1.setCosto(new Money("3"));
		r2.setCosto(new Money("3"));
		assertTrue(r1.equals(r2));
	}

}
