package domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class AtraccionTest {

	@Test(expected = IllegalArgumentException.class)
	public void when_construirConNombreNull_expect_exception() {
		Money costo = new Money("2");
		@SuppressWarnings("unused")
		Atraccion a = new Atraccion(null, costo);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_construirConMoneyNull_expect_exception() {
		@SuppressWarnings("unused")
		Atraccion a = new Atraccion("Atraccion", null);
	}
	
	@Test
	public void when_getNombre_expect_nombre() {
		String nombre = "Atraccion";
		Atraccion a = new Atraccion(nombre, new Money("2"));
		assertEquals(nombre, a.getNombre());
	}
	
	@Test
	public void when_getCostoPorDia_expect_money() {
		String nombre = "Atraccion";
		Money costo  = new Money("2");
		Atraccion a = new Atraccion(nombre, costo);
		assertEquals(costo, a.getCostoDeEntrada());
	}
	
	@Test
	public void when_dosAtraccionesTienenMismosAtributos_expect_equalHash() {
		String nombre1 = "Atraccion";
		String nombre2 = "Atraccion";
		Money costo1  = new Money("1");
		Money costo2  = new Money("1");
		Atraccion a1 = new Atraccion(nombre1, costo1);
		Atraccion a2 = new Atraccion(nombre2, costo2);
		assertTrue(a1.hashCode() == a2.hashCode());
	}
	
	@Test
	public void when_dosAtraccionesTienenMismosAtributos_expect_equalsTrue() {
		String nombre1 = "Atraccion";
		String nombre2 = "Atraccion";
		Money costo1  = new Money("1");
		Money costo2  = new Money("1");
		Atraccion a1 = new Atraccion(nombre1, costo1);
		Atraccion a2 = new Atraccion(nombre2, costo2);
		assertTrue(a1.equals(a2));
	}
	
	@Test
	public void when_AtraccionEqualsMismoObjeto_expect_equalsTrue() {
		String nombre = "Atraccion";
		Money costo  = new Money("4");
		Atraccion a = new Atraccion(nombre, costo);
		assertTrue(a.equals(a));
	}
	
	@Test
	public void when_AtraccionEqualsNull_expect_equalsFalse() {
		String nombre = "Atraccion";
		Money costo  = new Money("4");
		Atraccion a = new Atraccion(nombre, costo);
		assertFalse(a.equals(null));
	}
	
	@Test
	public void when_AtraccionEqualsInstanceOtraClase_expect_equalsFalse() {
		String nombre = "Atraccion";
		Money costo  = new Money("4");
		Atraccion a = new Atraccion(nombre, costo);
		assertFalse(a.equals(new String("")));
	}
	
	@Test
	public void when_AtraccionesDistintoCostoPorDiaIgualNombre_expect_equalsFalse() {
		String nombre1 = "Atraccion";
		String nombre2 = "Atraccion";
		Money costo1  = new Money("1");
		Money costo2  = new Money("2");
		Atraccion a1 = new Atraccion(nombre1, costo1);
		Atraccion a2 = new Atraccion(nombre2, costo2);
		assertFalse(a1.equals(a2));
	}
	
	@Test
	public void when_AtraccionesDistintoNombreIgualCostoPorDia_expect_equalsFalse() {
		String nombre1 = "Atraccion";
		String nombre2 = "nombre";
		Money costo1  = new Money("1");
		Money costo2  = new Money("1");
		Atraccion a1 = new Atraccion(nombre1, costo1);
		Atraccion a2 = new Atraccion(nombre2, costo2);
		assertFalse(a1.equals(a2));
	}

	@Test
	public void atraccionNula_test() {
	    assertEquals(Atraccion.getAtraccionNula(), Atraccion.getAtraccionNula());
	}
}
