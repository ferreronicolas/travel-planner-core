package domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;
public class MoneyTest {
	
	@Test
	public void constructorStringOkTest() {
		Money stringOk = new Money("10.50");
		assertEquals(new BigDecimal("10.50"), stringOk.getMonto());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void constructorStringMal1test() {
		@SuppressWarnings("unused")
		Money stringNo1 = new Money ("A.A");
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructorStringMal2Test() {
		@SuppressWarnings("unused")
		Money stringNo2 = new Money ("10.");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void stringnuloTest() {
		String cadenaNula = null;
		@SuppressWarnings("unused")
		Money dinero = new Money(cadenaNula);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void bdNuloTest() {
		BigDecimal bdNulo = null;
		@SuppressWarnings("unused")
		Money dinero = new Money(bdNulo);
	}
	
	@Test
	public void getTest() {
		Money dinero = new Money ("10.5");
		assertEquals(new BigDecimal ("10.5"), dinero.getMonto());
	}
	
	@Test
	public void toStringTest() {
		Money dinero = new Money("10.5");
		assertEquals("10.5", dinero.toString());
	}
	
	@Test
	public void hashCodeTest() {
		Money dinero = new Money ("10.50");
		Money dinero2 = new Money ("10.50");
		assertTrue(dinero.hashCode()==dinero2.hashCode());
	}
	
	@Test
	public void equalsTest() {
		Money dinero = new Money ("10.50");
		Money dinero2 = new Money ("10.50");
		assertTrue(dinero.equals(dinero2) && dinero.hashCode()==dinero2.hashCode());
	}
	
	@Test
	public void equalsSameTest() {
		Money dinero = new Money ("10.50");
		assertTrue(dinero.equals(dinero));
	}
	
	@Test
	public void equalsNullTest() {
		Money dinero = new Money ("10.50");
		Money dinero2 = null;
		assertFalse(dinero.equals(dinero2));
	}
	
	@Test
	public void equalsdistintaClaseTest() {
		Money dinero = new Money ("10.50");
		String cadena = "hola";
		assertFalse(dinero.equals(cadena));
	}
	@Test
	public void notEqualsTest() {
		Money dinero = new Money ("10.50");
		Money dinero2 = new Money ("20");
		assertFalse(dinero.equals(dinero2));
	}
	
	@Test
	public void sumar1Test() {
		Money dinero = new Money ("10.50");
		Money dinero2 = new Money ("10.50");
		assertEquals(new BigDecimal("21.00"), dinero.sumar(dinero2).getMonto());
	}
	
	@Test
	public void sumar2Test() {
		Money dinero = new Money ("10.50");
		Money dinero2 = new Money ("10");
		assertEquals(new BigDecimal("20.50"), dinero.sumar(dinero2).getMonto());

	}
	
	@Test
	public void restar1Test() {
		Money dinero = new Money ("10.50");
		Money dinero2 = new Money ("2");
		assertEquals(new BigDecimal("8.50"), dinero.restar(dinero2).getMonto());

	}
	
	@Test
	public void restar2Test() {
		Money dinero = new Money ("10.50");
		Money dinero2 = new Money ("11");
		assertEquals(new BigDecimal("-0.50"), dinero.restar(dinero2).getMonto());

	}
	
	@Test (expected = IllegalArgumentException.class)
	public void sumarNuloTest() {
		Money dinero = new Money ("10.50");
		Money dinero2 = null;
		dinero.sumar(dinero2);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void restarNuloTest() {
		Money dinero = new Money ("10.50");
		Money dinero2 = null;
		dinero.restar(dinero2);
	}

	@Test
	public void compareTo_test() {
	    Money dinero1 = new Money("10.50");
	    Money dinero2 = new Money("11.50");
	    assertEquals(-1, dinero1.compareTo(dinero2));
	    assertEquals(1, dinero2.compareTo(dinero1));
	    assertEquals(0, dinero1.compareTo(new Money("10.5")));
	}

	@Test
	public void montoDesconocido_test() {
	    Money desconocido = Money.MONTO_DESCONOCIDO;
	    Money dinero1     = new Money("10");
	    assertEquals(Money.MONTO_DESCONOCIDO, desconocido.sumar(dinero1));
	    assertEquals(Money.MONTO_DESCONOCIDO, desconocido.restar(dinero1));
	    assertEquals(dinero1, dinero1.sumar(desconocido));
	    assertEquals(dinero1, dinero1.restar(desconocido));
	    assertTrue("monto desconocido".equalsIgnoreCase(desconocido.toString()));
	}
}
