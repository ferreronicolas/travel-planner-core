package planificadores;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class PlanificadorFinderTest {

    private static String     classpath;
	private static File       dirInexistente;
	private static File       dirExistente;
	private static File       dirConClases;
	private static File       clase;
	private static File       archivoExistente;
	private static File       jarFile;
	private static File       jarCopiado;
	private static File       jarConPlanificador;
	private static JarFile    testJar;
	private static JarFile    testJarVacio;
	
	@BeforeClass
	public static void setUpClass() {
		classpath     = System.getProperty("java.class.path");
		dirInexistente = new File("dirInexistente");
		dirExistente   = new File("dirExistente");
		dirExistente.mkdir();
		archivoExistente = new File("archivoExistente.txt");
		try {
			archivoExistente.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Load jars
		ClassLoader cl  = PlanificadorFactoryTest.class.getClassLoader();
		URL urlTestJar  = cl.getResource("planificadores/myapp.jar");
		URL urlJarVacio = cl.getResource("planificadores/jarvacio.jar");
		URL urlJarPlan  = cl.getResource("planificadores/jarConPlanificador.jar");
		jarFile            = new File(urlTestJar.getFile());
		jarConPlanificador = new File(urlJarPlan.getFile());
		File jarVacioFile  = new File(urlJarVacio.getFile());
		try {
			testJar      = new JarFile(jarFile);
			testJarVacio = new JarFile(jarVacioFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@AfterClass
	public static void cleanUpClass() {
		dirExistente.delete();
		archivoExistente.delete();
	}
	
	private void crearDirJavaClasses() {
		dirConClases = new File("javaclasses");
		dirConClases.mkdir();
		agregarDirJavaClassesAClasspath();
	}
	
	private void borrarDirJavaClasses() {
		dirConClases.delete();
		restaurarClasspath();
	}
	
	private void agregarDirJavaClassesAClasspath() {
		String pathSepar = System.getProperty("path.separator");
		System.setProperty("java.class.path", 
				classpath + pathSepar + dirConClases.getAbsolutePath());
	}
	
	private void restaurarClasspath() {
		System.setProperty("java.class.path", classpath);
	}
    
	private void crearClassEnDirJavaClasses() {
		clase = new File(dirConClases.getPath() + System.clearProperty("file.separator") + "Clase.class");
		try {
			clase.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void borrarClassDeDirJavaClasses() {
		clase.delete();
	}
	
	private void agregarArchivoExistenteAClassPath() {
	    String pathSeparator = File.pathSeparator;
	    System.setProperty("java.class.path", 
	            classpath + pathSeparator + archivoExistente.getAbsolutePath()); 
	}

	private void copiarJarADirJavaClasses() {
	    try {
            jarCopiado = new File(dirConClases.getAbsolutePath() + File.separator + jarFile.getName());
            // copio el testJar a la carpeta javaclasses que está en classpath
            Files.copy(jarFile.toPath(), jarCopiado.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            Logger.getLogger(PlanificadorFinderTest.class.getName()).log(Level.SEVERE, null, e);
        }
	}
		
	private void copiarJarConPlanificadorADirJavaClasses() {
	    try {
            jarCopiado = new File(dirConClases.getAbsolutePath() + File.separator + jarConPlanificador.getName());
            // copio el testJar a la carpeta javaclasses que está en classpath
            Files.copy(jarConPlanificador.toPath(), jarCopiado.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            Logger.getLogger(PlanificadorFinderTest.class.getName()).log(Level.SEVERE, null, e);
        }
	}
	
    private void borrarJarDeDirJavaClasses() {
        // Borro el jar copiado de la carpeta javaclasses
        jarCopiado.delete();
    }

	// Tests para constructor
	
	@Test(expected = NullPointerException.class)
	public void when_dirNameEsNull_expect_exception() {
	    @SuppressWarnings("unused")
        PlanificadorFinder pf = new PlanificadorFinder(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_dirNameNoExisteEnClasspath_expect_exception() {
	    @SuppressWarnings("unused")
        PlanificadorFinder pf = new PlanificadorFinder("directorio");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void when_dirNameNoEsUnDirectorio_expect_exception() {
	    agregarArchivoExistenteAClassPath();
	    @SuppressWarnings("unused")
        PlanificadorFinder pf = new PlanificadorFinder(archivoExistente.getName());
	    restaurarClasspath();
	}
	
	@Test
	public void when_dirNameTieneUnJarInvalido_expect_URLsVacio() {
	    crearDirJavaClasses();
        try {
            File nuevoArchivo = new File(dirConClases.getAbsolutePath() + File.separator + "archivo.jar");
            nuevoArchivo.createNewFile();
            PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
            List<JarFile> jarsEncontrados = pf.buscarJarsEnDirectorio(dirConClases);
            assertEquals(0, jarsEncontrados.size());
            nuevoArchivo.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
        borrarDirJavaClasses();
	}

	// Test para getDirName

	@Test
	public void when_dirNameEsValido_expect_name() {
	    crearDirJavaClasses();
	    PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
	    assertEquals(dirConClases.getName(), pf.getDirName());
	    borrarDirJavaClasses();
	}

	// Test para getURLs

	@Test
	public void when_dirNameEsUnDirVacio_expect_URLsVacio() {
	    crearDirJavaClasses();
	    PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
	    assertEquals(0, pf.getUrls().length);
	    borrarDirJavaClasses();
	}
	
	@Test
	public void when_dirNameEsUnDirConUnArchivo_expect_URLsVacio() {
	    crearDirJavaClasses();
	    crearClassEnDirJavaClasses();
	    PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
	    assertEquals(0, pf.getUrls().length);
	    borrarClassDeDirJavaClasses();
	    borrarDirJavaClasses();
	}
	
	@Test
	public void when_dirNameEsUnDirConUnJar_expect_URLConUnElemento() {
	    crearDirJavaClasses();
	    copiarJarADirJavaClasses();
	    PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
	    assertEquals(1, pf.getUrls().length);
	    borrarJarDeDirJavaClasses();
	    borrarDirJavaClasses();
	}
	
    // Tests para método listarPlanificadores
	
	@Test
	public void when_noHayPlanificadoresEnDirAlListarPlanificadores_expect_listaVacia() {
	    crearDirJavaClasses();
	    PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
	    assertEquals(0, pf.listarPlanificadores().size());
	    borrarDirJavaClasses();
	}
	
	@Test
	public void when_hayUnJarSinPlanificadoresAlListarPlanificadores_expect_listaVacia() {
	    crearDirJavaClasses();
        copiarJarADirJavaClasses();
        PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
        assertEquals(0, pf.listarPlanificadores().size());
        borrarJarDeDirJavaClasses();
        borrarDirJavaClasses();
	}
	
	@Test
	public void when_hayUnPlanificadorAlListarPlanificadores_expect_listaConUnString() {
	    crearDirJavaClasses();
	    copiarJarConPlanificadorADirJavaClasses();
	    PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
	    assertEquals(1, pf.listarPlanificadores().size());
	    borrarJarDeDirJavaClasses();
	    borrarDirJavaClasses();
	}
	
	@Test
	public void when_seBorraElJarAntesDeListarPlanificadores_expect_listaVacia() {
	    crearDirJavaClasses();
	    copiarJarADirJavaClasses();
	    PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
	    borrarJarDeDirJavaClasses();
	    assertEquals(0, pf.listarPlanificadores().size());
	    borrarDirJavaClasses();
	}
    
    // Tests para método buscarJarsEnDirectorio
    
    @Test(expected = NullPointerException.class)
    public void when_dirEsNullAlBuscarJarsEnDir_expect_exception() {
        crearDirJavaClasses();
        PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
        pf.buscarJarsEnDirectorio(null);
        borrarDirJavaClasses();
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void when_dirNoEsDirectorioAlBuscarJarsEnDir_expect_exception() {
        crearDirJavaClasses();
        PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
        pf.buscarJarsEnDirectorio(archivoExistente);
        borrarDirJavaClasses();
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void when_dirNoExisteAlBuscarJarsEnDir_expect_exception() {
        crearDirJavaClasses();
        PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
        pf.buscarJarsEnDirectorio(dirInexistente);
        borrarDirJavaClasses();
    }
    
    @Test
    public void when_dirNoTieneJarsAlBuscarJarsEnDir_expect_listaVacia() {
        crearDirJavaClasses();
        PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
        List<JarFile> jarsEncontrados = pf.buscarJarsEnDirectorio(dirExistente);
        assertEquals(0, jarsEncontrados.size());
        borrarDirJavaClasses();
    }
    
    @Test
    public void when_dirTieneUnJarAlBuscarJarsEnDir_expect_ListaConUnJar() {
        crearDirJavaClasses();
        copiarJarADirJavaClasses();
        PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
        List<JarFile> jarsEncontrados = pf.buscarJarsEnDirectorio(dirConClases);
        assertEquals(1, jarsEncontrados.size());
        borrarJarDeDirJavaClasses();
        borrarDirJavaClasses();
    }
    
    @Test
    public void when_dirTieneUnArchivoJarPeroNoEsUnJarAlBuscarJarsEnDir_expect_listaVacia() {
        crearDirJavaClasses();
    	PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
    	File nuevoArchivo = new File(dirConClases.getAbsolutePath() + File.separator + "archivo.jar");
    	try {
			nuevoArchivo.createNewFile();
			List<JarFile> jarsEncontrados = pf.buscarJarsEnDirectorio(dirConClases);
			assertEquals(0, jarsEncontrados.size());
			nuevoArchivo.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	borrarDirJavaClasses();
    }

	// tests para método buscarClasesEnJar
	
	@Test(expected = NullPointerException.class)
	public void when_jarEsNull_expect_exception() {
	    crearDirJavaClasses();
		PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
		pf.buscarClasesEnJar(null);
		borrarDirJavaClasses();
	}
	
	@Test
	public void when_jarNoTieneClases_expect_listaVacia() {
	    crearDirJavaClasses();
		PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
		List<String> clasesEncontradas = pf.buscarClasesEnJar(testJarVacio);
		assertEquals(0, clasesEncontradas.size());
		borrarDirJavaClasses();
	}
	
	@Test
	public void when_jarTieneDosClases_expect_listConDosClases() {
	    crearDirJavaClasses();
		PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
		List<String> clases = pf.buscarClasesEnJar(testJar);
		assertEquals(2, clases.size());
		borrarDirJavaClasses();
	}
	
	@Test
	public void when_jarTieneClases_expect_listConNombresDeClasesValidos() {
	    crearDirJavaClasses();
		/* regex matchea nombres de paquetes válidos. Ej:
	     * Planificador (clase sin paquete)
	     * com.Clase (clase llamada Clase dentro del paquete com)
	     * com.myapp.Finder (clase Finder dentro de paquete myapp dentro de paquete com)
	     * com.planificadores.PluginFinder (nombre de la clase en camel case)
	     * El nombre de la clase siempre empieza con Mayúscula
		 */
		String regex = "([A-za-z]*\\.)*([A-Z][a-zA-Z]*)$";
		PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
		List<String> clases = pf.buscarClasesEnJar(testJar);
        clases.forEach(className -> assertTrue(className.matches(regex)));
        borrarDirJavaClasses();
	}

	@Test
	public void traducirNombreTest() {
	    crearDirJavaClasses();
        copiarJarConPlanificadorADirJavaClasses();
        PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
        Collection<String> planificadores = pf.listarPlanificadores();
        assertEquals(1, planificadores.size());
        Iterator<String> it = planificadores.iterator();
        String planificador = it.next();
        assertTrue("planificador de prueba".equalsIgnoreCase(planificador));
        assertEquals("pruebas.planificadores.PlanificadorDePrueba", pf.traducirNombre(planificador));
        borrarJarDeDirJavaClasses();
        borrarDirJavaClasses();
	}
}
