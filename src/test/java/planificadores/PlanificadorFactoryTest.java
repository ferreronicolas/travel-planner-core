package planificadores;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class PlanificadorFactoryTest {
	
    private static File dir;
    private PlanificadorAbstracto planificador = new PlanificadorNoEncontrado();
    
    @BeforeClass
    public static void setUpClass() {
        String dirNamePlanificadores = "planificadores";
        dir = new File(dirNamePlanificadores);
        dir.mkdir();
        System.setProperty("java.class.path", dir.getAbsolutePath());
        System.setProperty("dir.planificadores", dirNamePlanificadores);
    }

    @AfterClass
    public static void cleanUpClass() {
        dir.delete();
    }

    @Test(expected = NullPointerException.class)
    public void getPlanificadorNull_expect_nullPointerException() {
        PlanificadorAbstracto planner = PlanificadorFactory.getPlanificador(null);
        assertEquals(planificador, planner);
    }

    @Test
    public void getPlanificador_claseNoExistente() {
        PlanificadorAbstracto planner = PlanificadorFactory.getPlanificador("Clase");
        assertEquals(planificador, planner);
    }
 /*   
    @Test(expected = NullPointerException.class)
    public void when_classLoaderNull_expect_exception() {
        @SuppressWarnings("unused")
        PlanificadorFactory pf = new PlanificadorFactory(null);
    }
    
    @Test
    public void when_nombreDePlanificadorNull_expect_planificadorNulo() {
        PlanificadorFactory pf = new PlanificadorFactory(getClass().getClassLoader());
        Planificador planner = pf.getPlanificador(null);
        assertEquals(nullPlanner, planner);
    }
    
    @Test
    public void when_nombreDePlanificadorNoExiste_expect_planificadorNulo() {
        PlanificadorFactory pf = new PlanificadorFactory(getClass().getClassLoader());
        Planificador planner = pf.getPlanificador("com.myapp.Clase");
        assertEquals(nullPlanner, planner);
    }
    
    @Test
    public void when_nombreDePlanificadorNoEsPlanificador_expect_planificadorNulo() {
        PlanificadorFactory pf = new PlanificadorFactory(getClass().getClassLoader());
        Planificador planner = pf.getPlanificador(this.getClass().getCanonicalName());
        assertEquals(nullPlanner, planner);
    }
    
    @Test
    public void when_nombreDePlanificadorEsPlanificador_expect_planificadorDistintoNull() {
        URL url = getClass().getClassLoader().getResource("model/planificadores/planificadorLibrary.jar");
        URL[] urls = {url};
        URLClassLoader classLoader = new URLClassLoader(urls);
        PlanificadorFactory pf = new PlanificadorFactory(classLoader);
        Planificador planner = pf.getPlanificador("planificador.prueba.Library");
        assertNotEquals(nullPlanner, planner);
    }
    
	  @Test
	  public void ImplementaNoPlanifTest() {
		  String clase = "domain.Money";
		  PlanificadorFactory<Planificador> factory = new PlanificadorFactory<Planificador>(Planificador.class);
		  assertFalse(factory.implementaPlanificador(clase));
	  }
	  
	  @Test
	  public void ImplementaPlanifTest() {
		  String clase = "planificadores.PlanificadorTest";
		  PlanificadorFactory<Planificador> factory = new PlanificadorFactory<Planificador>(Planificador.class);
		  assertTrue(factory.implementaPlanificador(clase));
	  }
	  
	 @Test
	  public void getClassPlanifTest() throws ClassNotFoundException, InstantiationException, IllegalAccessException, ClassNotAssignableException {
		  String clase = "planificadores.PlanificadorTest";
		  PlanificadorFactory<Planificador> factory = new PlanificadorFactory<Planificador>(Planificador.class);
		  assertEquals(PlanificadorTest.class, factory.getClase(clase).getClass());
	  }
		
	  @Test(expected = IllegalArgumentException.class) 
	  public void getClaseNuloTest() throws ClassNotFoundException, InstantiationException, IllegalAccessException, ClassNotAssignableException { String clase = null;
		  Class<?> planif = null; 
		  @SuppressWarnings({ "unchecked", "rawtypes" })
		PlanificadorFactory factory = new PlanificadorFactory(planif);
		  @SuppressWarnings("unused")
		Class<?> clazz = (Class<?>) factory.getClase(clase); 
	  }
	  
		@Test (expected = ClassNotAssignableException.class)
		public void getClaseNoPlanifTest() throws ClassNotFoundException, InstantiationException, IllegalAccessException, ClassNotAssignableException {
			  String clase = "domain.Money";
			  PlanificadorFactory<Planificador> factory = new PlanificadorFactory<Planificador>(Planificador.class);
			  factory.getClase(clase);
		}
		
		@Test (expected = ClassNotFoundException.class)
		public void getClassNoExisTest() throws ClassNotFoundException, InstantiationException, IllegalAccessException, ClassNotAssignableException {
			  String clase = "hola";
			  PlanificadorFactory<Planificador> factory = new PlanificadorFactory<Planificador>(Planificador.class);
			  factory.getClase(clase);
		}
	  
	  
		@Test(expected = IllegalArgumentException.class)
		public void ImplementaNuloTest() {
			String clase = null;
			Class<?> planif = null;
			@SuppressWarnings({ "unchecked", "rawtypes" })
			PlanificadorFactory factory = new PlanificadorFactory(planif);
			factory.implementaPlanificador(clase);
		}
		
		@Test
		public void ImplementaNotFoundTest() {
			  String clase = "hola";
			  PlanificadorFactory<Planificador> factory = new PlanificadorFactory<Planificador>(Planificador.class);
			  assertFalse(factory.implementaPlanificador(clase));
		}
		
*/
	
}
