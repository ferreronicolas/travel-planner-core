package planificadores;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;

public class CriteriosDeAceptacion {

    private static String     classpath;
    private static File       dirConClases;
    private static File       jarConPlanificador;
    private static File       jarCopiado;
    private static File       jarFile;

    @BeforeClass
    public static void setUpClass() {
        classpath     = System.getProperty("java.class.path");
        ClassLoader cl  = PlanificadorFactoryTest.class.getClassLoader();
        URL urlJarPlan  = cl.getResource("planificadores/jarConPlanificador.jar");
        URL urlTestJar  = cl.getResource("planificadores/myapp.jar");
        jarConPlanificador = new File(urlJarPlan.getFile());
        jarFile            = new File(urlTestJar.getFile());
    }
    
    @Test(expected = NullPointerException.class)
    public void caso1Test() {
        @SuppressWarnings("unused")
        PlanificadorFinder pf = new PlanificadorFinder(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void caso2Test() {
        @SuppressWarnings("unused")
        PlanificadorFinder pf = new PlanificadorFinder("directorio");
    }
    
    @Test
    public void caso3Test() {
        crearDirJavaClasses();
        PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
        assertEquals(0, pf.listarPlanificadores().size());
        borrarDirJavaClasses();
    }
    
    @Test
    public void caso4Test() {
        crearDirJavaClasses();
        File archivoTxt = new File(dirConClases.getAbsolutePath() + File.separator + "archivo.txt");
        try {
            archivoTxt.createNewFile();
        } catch (IOException e) {
            fail("No se pudo crear el archivo.");
        }
        PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
        assertEquals(0, pf.listarPlanificadores().size());
        archivoTxt.delete();
        borrarDirJavaClasses();
    }
    
    @Test
    public void caso5Test() {
        crearDirJavaClasses();
        copiarJarADirJavaClasses();
        PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
        Collection<String> planificadoresEncontrados = pf.listarPlanificadores();
        assertEquals(0, planificadoresEncontrados.size());
        borrarJarDeDirJavaClasses();
        borrarDirJavaClasses();
    }

    @Test
    public void caso6Test() {
        crearDirJavaClasses();
        copiarJarConPlanificadorADirJavaClasses();
        PlanificadorFinder pf = new PlanificadorFinder(dirConClases.getName());
        Collection<String> planificadoresEncontrados = pf.listarPlanificadores();
        assertEquals(1, planificadoresEncontrados.size());
        assertEquals("Planificador de prueba", planificadoresEncontrados.iterator().next());
        borrarJarDeDirJavaClasses();
        borrarDirJavaClasses();
    }

    @Test(expected = NullPointerException.class)
    public void caso7Test() {
        PlanificadorFactory.getPlanificador(null);
    }

    @Test
    public void caso8Test() {
        PlanificadorAbstracto planificador = PlanificadorFactory.getPlanificador("planificador.PlanificadorMenorCosto");
        assertEquals(PlanificadorNoEncontrado.class, planificador.getClass());
    }

    @Test
    public void caso9Test() {
        crearDirJavaClasses();
        copiarJarADirJavaClasses();
        // El jar copiado al dir javaclasses tiene una clase con este nombre binario
        String binaryName = "com.myapp.Persona";
        PlanificadorAbstracto planificador = PlanificadorFactory.getPlanificador(binaryName);
        assertEquals(PlanificadorNoEncontrado.class, planificador.getClass());
        borrarJarDeDirJavaClasses();
        borrarDirJavaClasses();
    }

    @Test
    public void caso10Test() {
        crearDirJavaClasses();
        copiarJarConPlanificadorADirJavaClasses();
        // El jar copiado a dir javaclasses tiene un planificador con este nombre binario
        String binaryName = "pruebas.planificadores.PlanificadorDePrueba";
        System.setProperty("dir.planificadores", dirConClases.getName());
        PlanificadorAbstracto.cargarFinder();
        PlanificadorAbstracto planificador = PlanificadorFactory.getPlanificador(binaryName);
        assertNotEquals(PlanificadorNoEncontrado.class, planificador.getClass());
        assertEquals(binaryName, planificador.getClass().getCanonicalName());
        borrarJarDeDirJavaClasses();
        borrarDirJavaClasses();
    }

    private void crearDirJavaClasses() {
        dirConClases = new File("javaclasses");
        dirConClases.mkdir();
        agregarDirJavaClassesAClasspath();
    }
    
    private void agregarDirJavaClassesAClasspath() {
        String pathSepar = System.getProperty("path.separator");
        System.setProperty("java.class.path", 
                classpath + pathSepar + dirConClases.getAbsolutePath());
    }
    
    private void borrarDirJavaClasses() {
        dirConClases.delete();
        restaurarClasspath();
    }
    
    private void restaurarClasspath() {
        System.setProperty("java.class.path", classpath);
    }
    
    private void copiarJarADirJavaClasses() {
        try {
            jarCopiado = new File(dirConClases.getAbsolutePath() + File.separator + jarFile.getName());
            // copio el testJar a la carpeta javaclasses que está en classpath
            Files.copy(jarFile.toPath(), jarCopiado.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            Logger.getLogger(PlanificadorFinderTest.class.getName()).log(Level.SEVERE, null, e);
        }
    }
        
    private void copiarJarConPlanificadorADirJavaClasses() {
        try {
            jarCopiado = new File(dirConClases.getAbsolutePath() + File.separator + jarConPlanificador.getName());
            // copio el testJar a la carpeta javaclasses que está en classpath
            Files.copy(jarConPlanificador.toPath(), jarCopiado.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            Logger.getLogger(PlanificadorFinderTest.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void borrarJarDeDirJavaClasses() {
        // Borro el jar copiado de la carpeta javaclasses
        jarCopiado.delete();
    }
}
